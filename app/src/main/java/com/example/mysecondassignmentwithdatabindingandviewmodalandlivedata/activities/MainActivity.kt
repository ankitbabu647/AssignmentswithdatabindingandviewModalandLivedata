
// this is my Package Name which contains my Project all files
package com.example.mysecondassignmentwithdatabindingandviewmodalandlivedata.activities


// Here I Add All Essential Librarie and Classes And Packages
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.mysecondassignmentwithdatabindingandviewmodalandlivedata.R
import com.example.mysecondassignmentwithdatabindingandviewmodalandlivedata.databinding.ActivityMainBinding
import com.example.mysecondassignmentwithdatabindingandviewmodalandlivedata.viewModal.ViewModalemoActivityViewModal


// this is My Main thread Means Main Activity and this extends superClass AppCompatActivity
class MainActivity : AppCompatActivity(), View.OnClickListener {

    // Here I Declare DataBinding Class Reference variable
    private lateinit var binding:ActivityMainBinding


    // Here I Declare viewModal Class Reference Variable
    private lateinit var viewModal: ViewModalemoActivityViewModal


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Here I Initialization of Databinding reference variable with dataBinding Class Object object creating below
        binding=DataBindingUtil.setContentView(this@MainActivity,R.layout.activity_main)


        // Here I Initialization of viewModal  reference variable with viewModalProvider This is Default factory Class to Create Our viewModal Class Object object creating below
        viewModal= ViewModelProvider(this)[ViewModalemoActivityViewModal::class.java]

        // Here I Take viewModal for lifecyle this means Current Class Object because viewModal class Associated with an Activity
        binding.lifecycleOwner=this

        //  Here I Assign in myViewModal Variable  My Main ViewModal Class Object then My View and viewModal class will be attech myViewModal variable is xml file variable
        binding.myViewModal=viewModal


        // Here I Take Click Event on Button
        binding.btnAdd.setOnClickListener(this)

        // Here I use Obserever Class of function to up to date data because Everytime My Data will be Up to Date
        viewModal.totleCount.observe(this) {
            // Here I Set Current value on TextView
            binding.tvDisplay.text = it.toString()
        }

    }

    override fun onClick(v: View?) {
        // Here i get input from user with the Help of EditText i Store it in viewModal Class
        viewModal.updateCount(binding.etInput.editText?.text.toString().toInt())
    }
}